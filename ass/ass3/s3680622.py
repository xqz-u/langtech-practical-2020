#!/usr/bin/env python3
import re
from fileinput import input
from string import Template

from requests import HTTPError, get

default_questions = [
    "What are the effects of a tsunami?",
    "Who are the crew members of the Apollo 11?",
    "What is the taxon name of a wolf?",
    "What is the scientific name of the great white shark?",
    "What is the surface gravity of the moon?",
    "What is the opposite of a carnivore?",
    "What is the perimeter of the earth?",
    "What is the orbital inclination of the ISS?",
    "What is the antiparticle of the quark?",
    "Who are the inventors of the A* search algorithm?"
]

SPARQL_query_template = Template(
    """SELECT ?var ?varLabel WHERE { wd:$entity wdt:$property ?var. SERVICE
    wikibase:label { bd:serviceParam wikibase:language "en". } }""")

default_pattern = (r"(?:What|Who)\s+(?:is|are)\s+(?:a|the)\s+(?P<obj>.*)\s+of"
                   r"\s+(?:the|a)\s+(?P<subj>.*)")

default_regex = re.compile(default_pattern)

SPARQL_ENTRYPOINT = r"https://query.wikidata.org/sparql"

Action_API_URL = r"https://www.wikidata.org/w/api.php"

search_params = {'language': 'en', 'format': 'json'}


def safe_request(URL: str, args: dict) -> dict:
    data = None
    try:
        data = get(URL, params=args)
        data.raise_for_status()
    except HTTPError as http_err:
        print(f"HTTP error! -> {http_err}")
    except Exception as err:
        print(f"Request error! -> {err}")
    return data.json() if data else None


def print_answer(answer_dict: dict):
    # Following condition should be reached only when both Q: and P: were
    # retrieved from the search API, but they are not the correct ones to
    # satisfy this query
    if (not answer_dict['results']['bindings']):
        return print("Answer not found!")
    for result in answer_dict['results']['bindings']:
        for var in result:
            value = result[var]['value']
            if (re.search("Label", var)):
                print(f"{value}")


def match_subj_obj(question: str) -> tuple:
    match = default_regex.search(question)
    if not match:
        return None
    subj, obj = match.group('subj'), match.group('obj')
    subj = subj[:-1] if subj[-1:] == "?" else subj[:-2] if subj[
        -2:] == " ?" else subj  # Allow optional ending question mark &
    # whitespace (better to use regex next time...)
    obj = obj[:-1] if obj[-1:] in ('s', 'es') else obj  # Strip plural
    # print(subj, obj)
    return subj, obj


def search_wikidata_db(resource: str, **kwargs: str) -> str:
    params_cp = search_params.copy()
    for k, v in kwargs.items():
        params_cp[k] = v
    params_cp['search'] = resource
    wikidata_resource = safe_request(Action_API_URL, params_cp).get('search')
    return None if not wikidata_resource else wikidata_resource[0].get(
        'id')  # Rn match only first result returned...


def set_SPARQL_query(SPARQL_URIs: tuple):
    entity_URI, prop_URI = SPARQL_URIs
    templated_query = SPARQL_query_template.substitute(entity=entity_URI,
                                                       property=prop_URI)
    search_params['query'] = templated_query


def query_Action_API(user_question: str) -> tuple:
    matched_subj, matched_obj = match_subj_obj(user_question) or (None, None)
    if not (matched_subj and matched_obj):
        return None
    matched_subj_URI, matched_obj_URI = search_wikidata_db(
        matched_subj), search_wikidata_db(matched_obj, type='property')
    return None if not (matched_subj_URI and
                        matched_obj_URI) else matched_subj_URI, matched_obj_URI


def create_and_fire_query(user_in: str) -> str:
    search_params['action'] = 'wbsearchentities'
    wikidata_subj_URI, wikidata_obj_URI = query_Action_API(user_in) or (None,
                                                                        None)
    if not (wikidata_subj_URI and wikidata_obj_URI):
        return print("Answer not found!")
    search_params.pop('action')
    set_SPARQL_query((wikidata_subj_URI, wikidata_obj_URI))
    query_result = safe_request(SPARQL_ENTRYPOINT, search_params)
    return print("Answer not found!") if not query_result else print_answer(
        query_result)


def show_available_queries():
    for idx, question in enumerate(default_questions):
        print(f"({idx}) {question}")


def main():
    show_available_queries()
    for line in input():
        create_and_fire_query(line.rstrip())
        print("=========================================")
        show_available_queries()


if (__name__ == "__main__"):
    main()
