#!/usr/bin/env python3
from re import search
from sys import argv

from numpy import datetime64
from requests import HTTPError, get

from pandas import to_datetime

questions = [
    "When was the Turing Award instituted?",
    "What is Plutarch's most famous body of work?",
    "What are the material results of the Manhattan Project?",
    "Who are the inventors of Deep Learning?", "Who wrote the Leviathan?",
    "Who discovered the electron?", "What chemical elements form penicillin?",
    "How old is the universe?",
    "When did the specie Homo Neanderthalensis go extinct?",
    "When did the specie Homo Sapiens first appear on Earth?"
]

queries = [
    """SELECT ?year ?yearLabel WHERE { wd:Q185667 wdt:P571 ?year.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en".
    } }""", """SELECT ?opera ?operaLabel WHERE { wd:Q41523 wdt:P800 ?opera.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". } }""",
    """SELECT ?bombs ?bombsLabel WHERE { wd:Q127050 wdt:P1056 ?bombs. SERVICE
    wikibase:label { bd:serviceParam wikibase:language "en". } }""", """SELECT
    ?dl ?dlLabel WHERE { wd:Q197536 wdt:P61 ?dl. SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en". } }""", """SELECT ?author
    ?authorLabel WHERE { wd:Q193034 wdt:P50 ?author. SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en". } }""", """SELECT ?author
    ?authorLabel WHERE { wd:Q2225 wdt:P61 ?author. SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en". } }""", """SELECT ?component
    ?componentLabel WHERE { wd:Q12190 wdt:P527 ?component. SERVICE
    wikibase:label { bd:serviceParam wikibase:language "en". } }""",
    """SELECT ?age ?ageLabel WHERE { wd:Q1 wdt:P580 ?age. SERVICE wikibase:label
    { bd:serviceParam wikibase:language "en". } }""", """SELECT ?extinctionTime
    ?extinctionTimeLabel WHERE { wd:Q40171 wdt:P582 ?extinctionTime. SERVICE
    wikibase:label { bd:serviceParam wikibase:language "en". } }""",
    """SELECT ?birthTime ?birthTimeLabel WHERE { wd:Q15978631 wdt:P580
    ?birthTime. SERVICE wikibase:label { bd:serviceParam wikibase:language "en".
    } }"""
]


def parse_XSD_dateTime(xsd_dt_val: str) -> str:
    tmp_dt_val = xsd_dt_val[:-1] if xsd_dt_val[-1] == 'Z' else xsd_dt_val
    np_dt_obj = datetime64(tmp_dt_val)
    if (str(np_dt_obj)[0] == '-'):
        neg_year = int(str(np_dt_obj).split('-')[1])
        return f"{neg_year//1000000} million years BCE" if neg_year >= 1000000 else f"{neg_year} BCE"
    return to_datetime(str(np_dt_obj)).strftime("%w %B %Y")


def safe_request(query: str) -> dict:
    SPARQL_ep = "https://query.wikidata.org/sparql"
    data = None
    try:
        data = get(SPARQL_ep, params={"query": query, "format": "json"})
        data.raise_for_status()
    except HTTPError as http_err:
        print(f"HTTP error! -> {http_err}")
    except Exception as err:
        print(f"Request error! -> {err}")
    return data.json() if data else data


def print_answer(answer_dict: dict):
    flag = False
    for result in answer_dict['results']['bindings']:
        for var in result:
            value = result[var]['value']
            num_value_check = result[var].get('datatype')
            if (num_value_check and search("dateTime", num_value_check)):
                print(parse_XSD_dateTime(value))
                flag = not flag
            else:
                if (search("Label", var) and not flag):
                    print(f"{value}")
                    flag = not flag
        flag = False


def retrieve_answer(question_nums: list):
    for n in question_nums:
        if (n >= len(questions)):
            print(
                f"***Error! Question {n + 1} does not exist, we have {len(questions)} in total***"
            )
            continue
        print(questions[n])
        answer = safe_request(queries[n])
        if (answer):
            print_answer(answer)


def main():
    if (len(argv) < 2):
        print(f"Usage: {argv[0]} + query_number [query number...]")
        exit(1)

    retrieve_answer([int(x) - 1 for x in argv[1:]])


if __name__ == "__main__":
    main()
