#!/usr/bin/env python

from fileinput import input

import spacy
from requests import HTTPError, get
from spacy.symbols import ADP, AUX, DET, PRON, PUNCT, VERB, dobj, nsubj
from spacy.tokens import Doc, Token

# global declarations
nlp = spacy.load('en_core_web_sm')  # import SpaCy English model

SPARQL_ENTRYPOINT = r"https://query.wikidata.org/sparql"

Action_API_URL = r"https://www.wikidata.org/w/api.php"

search_params = {
    'language': 'en',
    'format': 'json'
}  # general params for every HTTP request performed

# user agent header requested by WikiData
user_agent_header_str = (
    r"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 "
    r"(KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")

user_agent_header = {'User-Agent': user_agent_header_str}

# new default test set
ass4_default_question = [
    "Who invented penicillin?", "What did Alexander Fleming discover?",
    "What does the polio cause?", "What did Albert Einstein discover?",
    "Who was the inventor of the automobile?",
    "Who are the founders of Google?", "Who authored the Iliad?",
    "What is the taxon name of a wolf", "What did Hemingway write?",
    "What does the flu cause?"
]

# dummy map of WikiData property to known verbs lemma
known_props_map = {'author': ['writer']}

# list of verbs coming from Latin for which the agent noun suffix is -or
agent_noun_latin_suff = ["invent", "conquer"]


def interpolate_query(ent: str, prop: str, rev: bool) -> str:
    query_triplet = ((f"wd:{ent} wdt:{prop} ?var") if not rev else
                     (f"?var wdt:{prop} wd:{ent}"))
    return (f"SELECT ?var ?varLabel WHERE {{ {query_triplet}. SERVICE wikibase"
            f":label {{ bd:serviceParam wikibase:language 'en'. }} }}")


def update_dict(dictionary: dict, **kwargs) -> dict:
    return {**dictionary, **kwargs}


def show_available_queries(questions=ass4_default_question):
    for idx, question in enumerate(questions):
        print(f"({idx}) {question}")
    print()


def question_not_implemented() -> None:
    print("Question format not implemented yet!")
    return None


def print_answer(answer_dict: dict):
    # following condition is reached only when both Q: and P: were
    # retrieved from the search API - but they are not the correct ones to
    # satisfy this query
    if not answer_dict['results']['bindings']:
        return print("Answer not found!")
    for result in answer_dict['results']['bindings']:
        print(result['varLabel']['value'])


def safe_request(URL: str, params: dict, **kwargs: dict) -> dict:
    data = None
    try:
        data = get(URL, params=params, **kwargs)
        data.raise_for_status()
    except HTTPError as http_err:
        print(f"HTTP error! -> {http_err}")
    except Exception as err:
        print(f"Request error! -> {err}")
    return data.json() if data else None


def search_wikidata_db(resource: str, **kwargs: str) -> str:
    params = update_dict(search_params,
                         action='wbsearchentities',
                         search=resource,
                         **kwargs)
    wikidata_resource = safe_request(Action_API_URL, params).get('search')
    # match only first result returned :/
    return None if not wikidata_resource else wikidata_resource[0].get('id')


def extract_interr_nounph_from_syndep(tokens: list, filter_tags: set) -> list:
    return ([
        noun_ph[1:] if noun_ph[0].pos == DET else noun_ph for noun_ph in tokens
        if (noun_ph.root.pos != PRON) and (
            noun_ph.root.head.pos in filter_tags)
    ])


def wh_question_nounphrase(tokens: Doc, root_pos_tags: set) -> str:
    match_nounph = extract_interr_nounph_from_syndep(tokens.noun_chunks,
                                                     root_pos_tags)
    return " ".join(w.lemma_ for phrase in match_nounph for w in phrase)


def parse_aux_based_q(question_toks: Doc) -> tuple:
    """
    Parse questions of the form 'who/what is/are/were/ (the) X of Y?'
    """
    subject_nph_root_dep = (ADP, None)
    object_nph_root_dep = (VERB, AUX)
    return (wh_question_nounphrase(question_toks, subject_nph_root_dep),
            wh_question_nounphrase(question_toks, object_nph_root_dep), False)


def property_synonym(lemma: str) -> str:
    for k, v in known_props_map.items():
        for n in v:
            if n == lemma:
                return k
    return None


def form_agent_noun(lemma: str) -> str:
    if lemma[-2:] == "or":  # quick fix for already (Latin) agentified lemmas
        return lemma
    lemma = (lemma + "or" if lemma in agent_noun_latin_suff else
             (lemma if lemma[-1] != 'e' else lemma[:-1]) + "er")
    fix_missing_prop_wd = property_synonym(lemma)
    return lemma if not fix_missing_prop_wd else fix_missing_prop_wd


def prop_from_verb(lemma: str) -> str:
    approach_0 = property_synonym(lemma)
    return form_agent_noun(lemma) if not approach_0 else approach_0


def parse_leftmost_trans_q(verb_tok: Token) -> tuple:
    """Parse questions of the form 'who/what TRANS_VERB someone/something'"""
    dir_obj = next(verb_tok.rights, None)  # dobj is first rightmost child
    if not dir_obj:
        question_not_implemented()
    subj = " ".join(
        [t.text for t in dir_obj.subtree if t.pos not in (DET, PUNCT)])
    obj = prop_from_verb(str(verb_tok.lemma_))
    return subj, obj, False


def parse_rightmost_trans_q(verb_tok: Token) -> tuple:
    """
    Parse questions of the form 'who/what AUX TRANS_VERB someone/something'
    NOTE right now just reverse property semantics extracted by the verb (e.g.
    What causes polio? -> ?x :cause :polio)
    """
    subj = " ".join([ent.text for ent in verb_tok.doc.ents]) or None
    if not subj:
        for t in verb_tok.lefts:
            subj = t.text if t.dep == nsubj else None
    if not subj:
        return question_not_implemented()
    verb_basename = str(verb_tok.lemma_)
    ents_type = (ent.label_ for ent in verb_tok.doc.ents)
    obj = verb_basename if not (next(ents_type,
                                     None)) else prop_from_verb(verb_basename)
    return subj, obj, True


def is_single_aux_verb(tok: Token) -> bool:
    return (tok.pos == AUX) and (not tok.head.pos == VERB)


def find_question_format(question: str) -> tuple:
    doc = nlp(question)
    for w in doc:
        if is_single_aux_verb(w):
            # in English an auxiliary should always precedes its verbal root,
            # so catch these formulations first
            return parse_aux_based_q(doc)
        if w.pos == VERB:
            leftmost_child = next(w.lefts, None)
            # aka not a WP; analyze syntax and not POS tags rn
            if leftmost_child and leftmost_child.dep not in (nsubj, dobj):
                return question_not_implemented()
            if leftmost_child.dep == nsubj:
                return parse_leftmost_trans_q(w)
            return parse_rightmost_trans_q(w)
    return question_not_implemented()


def query_Action_API(user_question: str) -> tuple:
    matched_subj, matched_obj, rev = (find_question_format(user_question)
                                      or (None, None, None))
    if not (matched_subj and matched_obj):
        return None
    matched_subj_URI, matched_obj_URI = search_wikidata_db(
        matched_subj), search_wikidata_db(matched_obj, type='property')
    return (None if not (matched_subj_URI and matched_obj_URI) else
            matched_subj_URI, matched_obj_URI, rev)


def create_and_fire_query(user_in: str):
    wikidata_subj_URI, wikidata_obj_URI, reverse_query = (
        query_Action_API(user_in) or (None, None, None))
    if not (wikidata_subj_URI and wikidata_obj_URI):
        return print("Answer not found!")
    params = update_dict(search_params,
                         query=interpolate_query(wikidata_subj_URI,
                                                 wikidata_obj_URI,
                                                 reverse_query))
    query_result = safe_request(SPARQL_ENTRYPOINT,
                                params,
                                headers=user_agent_header)
    print("Answer not found!") if not query_result else print_answer(
        query_result)


def main():
    show_available_queries()
    for line in input():
        create_and_fire_query(line.rstrip())
        print("=========================================")


if (__name__ == "__main__"):
    main()
