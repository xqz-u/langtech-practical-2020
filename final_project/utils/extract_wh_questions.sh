#!/bin/bash

questions_file=all_questions_sorted.txt
wh_words=(what who when where whose how)
declare questions

{ [ -e $questions_file ] && questions=$(< $questions_file); } || {
    echo "$questions_file not found here!" && exit 1
}

function create_wh_textfile() {
    file_var=$1
    shift 1
    for arg in "$@"
    do
        grep -i -E "^$arg .*$" <<< "$file_var" > "$arg"_q.txt
    done
}

create_wh_textfile "$questions" "${wh_words[@]}"
# quick fix for whom as there will be no phrases using it...
grep -i whom <<< "$questions" > whom_q.txt
