# found at https://shirishkadam.com/2016/12/23/dependency-parsing-in-nlp/
# to visualize dependency trees in nltk style (?)

import spacy
from nltk.tree import Tree


def tok_format(tok):
    return "_".join([tok.orth_, tok.tag_, tok.dep_])


def to_nltk_tree(node):
    if node.n_lefts + node.n_rights > 0:
        return Tree(tok_format(node),
                    [to_nltk_tree(child) for child in node.children])
    else:
        return tok_format(node)


command = "Who are Einstein's children?"
en_nlp = spacy.load('en_core_web_sm')
en_doc = en_nlp(u'' + command)

[to_nltk_tree(sent.root).pretty_print() for sent in en_doc.sents]
